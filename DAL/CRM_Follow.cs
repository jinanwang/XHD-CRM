﻿/*
* CRM_Follow.cs
*
* 功 能： N/A
* 类 名： CRM_Follow
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-22 16:33:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using XHD.DBUtility;//Please add references
namespace XHD.DAL
{
    /// <summary>
    /// 数据访问类:CRM_Follow
    /// </summary>
    public partial class CRM_Follow
    {
        public CRM_Follow()
        { }
        #region  BasicMethod

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(XHD.Model.CRM_Follow model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into CRM_Follow(");
            strSql.Append("Customer_id,Contact_id,Follow_aim_id,Follow,Follow_date,Follow_Type_id,department_id,employee_id,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append("@Customer_id,@Contact_id,@Follow_aim_id,@Follow,@Follow_date,@Follow_Type_id,@department_id,@employee_id,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@Customer_id", SqlDbType.Int,4),
					new SqlParameter("@Contact_id", SqlDbType.Int,4),
					new SqlParameter("@Follow_aim_id", SqlDbType.Int,4),
					new SqlParameter("@Follow", SqlDbType.VarChar,4000),
					new SqlParameter("@Follow_date", SqlDbType.DateTime),
					new SqlParameter("@Follow_Type_id", SqlDbType.Int,4),
					new SqlParameter("@department_id", SqlDbType.Int,4),
					new SqlParameter("@employee_id", SqlDbType.Int,4),
					new SqlParameter("@isDelete", SqlDbType.Int,4),
					new SqlParameter("@Delete_time", SqlDbType.DateTime)};
            parameters[0].Value = model.Customer_id;
            parameters[1].Value = model.Contact_id;
            parameters[2].Value = model.Follow_aim_id;
            parameters[3].Value = model.Follow;
            parameters[4].Value = model.Follow_date;
            parameters[5].Value = model.Follow_Type_id;
            parameters[6].Value = model.department_id;
            parameters[7].Value = model.employee_id;
            parameters[8].Value = model.isDelete;
            parameters[9].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(XHD.Model.CRM_Follow model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update CRM_Follow set ");
            strSql.Append("Contact_id=@Contact_id,");
            strSql.Append("Follow_aim_id=@Follow_aim_id,");
            strSql.Append("Follow=@Follow,");
            strSql.Append("Follow_Type_id=@Follow_Type_id");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@Contact_id", SqlDbType.Int,4),
					new SqlParameter("@Follow_aim_id", SqlDbType.Int,4),
					new SqlParameter("@Follow", SqlDbType.VarChar,4000),
					new SqlParameter("@Follow_Type_id", SqlDbType.Int,4),
					new SqlParameter("@id", SqlDbType.Int,4)};

            parameters[0].Value = model.Contact_id;
            parameters[1].Value = model.Follow_aim_id;
            parameters[2].Value = model.Follow;
            parameters[3].Value = model.Follow_Type_id;
            parameters[4].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int id)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CRM_Follow ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CRM_Follow ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select [id],[Customer_id],[Follow],[Follow_date],[Follow_Type_id],[department_id],[employee_id] ,[isDelete],[Delete_time],Follow_aim_id,Contact_id ");
            strSql.Append(",( select Customer from CRM_Customer where id=CRM_Follow.[Customer_id]) as Customer_name ");
            strSql.Append(",( select params_name from Param_SysParam where id = CRM_Follow.[Follow_Type_id]) as Follow_Type ");
            strSql.Append(",(select d_name from hr_department where id=CRM_Follow.[department_id]) as [Department] ");
            strSql.Append(",(select name from hr_employee where ID=CRM_Follow.[employee_id]) as[Employee] ");
            strSql.Append(",( select C_name from CRM_Contact where id = CRM_Follow.Contact_id) as Contact ");
            strSql.Append(",( select params_name from Param_SysParam where id = CRM_Follow.Follow_aim_id) as Follow_aim ");
            strSql.Append(" FROM CRM_Follow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" [id],[Customer_id],[Follow],[Follow_date],[Follow_Type_id],[department_id],[employee_id] ,[isDelete],[Delete_time],Follow_aim_id,Contact_id ");
            strSql.Append(",( select Customer from CRM_Customer where id=CRM_Follow.[Customer_id]) as Customer_name ");
            strSql.Append(",( select params_name from Param_SysParam where id = CRM_Follow.[Follow_Type_id]) as Follow_Type ");
            strSql.Append(",(select d_name from hr_department where id=CRM_Follow.[department_id]) as [Department] ");
            strSql.Append(",(select name from hr_employee where ID=CRM_Follow.[employee_id]) as[Employee] ");
            strSql.Append(",( select C_name from CRM_Contact where id = CRM_Follow.Contact_id) as Contact ");
            strSql.Append(",( select params_name from Param_SysParam where id = CRM_Follow.Follow_aim_id) as Follow_aim ");
            strSql.Append(" FROM CRM_Follow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();
            strSql_total.Append(" select count(id) FROM CRM_Follow ");
            strSql_grid.Append(" SELECT ");
            strSql_grid.Append("     n,[id],[Customer_id],[Follow],[Follow_date],[Follow_Type_id],[department_id],[employee_id] ,[isDelete],[Delete_time],Follow_aim_id,Contact_id ");
            strSql_grid.Append("     ,( select Customer from CRM_Customer where id=w1.[Customer_id]) as Customer_name ");
            strSql_grid.Append("     ,( select params_name from Param_SysParam where id = w1.[Follow_Type_id]) as Follow_Type  ");
            strSql_grid.Append("     ,(select d_name from hr_department where id=w1.[department_id]) as [Department] ");
            strSql_grid.Append("     ,(select name from hr_employee where ID=w1.[employee_id]) as[Employee]        ");
            strSql_grid.Append("     ,( select C_name from CRM_Contact where id = w1.Contact_id) as Contact ");
            strSql_grid.Append("     ,( select params_name from Param_SysParam where id = w1.Follow_aim_id) as Follow_aim ");
            strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from [dbo].[CRM_Follow]");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" where " + strWhere);
                strSql_total.Append(" where " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder + "   ");
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod
        #region  ExtensionMethod

        public DataSet Reports_year(string items, int year, string where)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if OBJECT_ID('Tempdb..#t') is not null ");
            strSql.Append("    drop TABLE  #t ");
            //strSql.Append("go");
            strSql.Append(" begin ");
            //strSql.Append("    --预统计表 #t");
            strSql.Append("    select ");
            strSql.Append("        params_name,'m'+convert(varchar,month(Follow_date)) mm,count(id)tNum into #t ");
            strSql.Append("   from (select w1.*,(select params_name from Param_SysParam where id = w1." + items + "_id) as params_name from CRM_Follow w1 ");
            strSql.Append("    where datediff(YEAR,w1.[Follow_date],'" + year + "-1-1')=0 ) w2 ");
            if (where.Trim() != "")
            {
                strSql.Append(" and " + where);
            }
            strSql.Append("    group by w2.params_name,'m'+convert(varchar,month(w2.Follow_date)) ");

            //strSql.Append("    --生成SQL");
            strSql.Append("    declare @sql varchar(8000) ");
            strSql.Append("    set @sql='select params_name items ' ");
            strSql.Append("    select @sql = @sql + ',sum(case mm when ' + char(39) +mm+ char(39) + ' then tNum else 0 end) ['+ mm +']' ");
            strSql.Append("        from (select distinct mm from #t)as data ");
            strSql.Append("    set @sql = @sql + ' from #t group by params_name' ");

            strSql.Append("    exec(@sql) ");
            strSql.Append(" end ");
            //strSql.Append("go");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 客户跟进【同比环比】
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <returns></returns>
        public DataSet Compared_follow(string year1, string month1, string year2, string month2)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select Follow_Type as yy, count(Follow_Type)as xx,");
            strSql.Append(" SUM(case when YEAR( Follow_date)=('" + year1 + "') and MONTH(Follow_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( Follow_date)=('" + year2 + "') and MONTH(Follow_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" FROM (select (select params_name from Param_SysParam where id = CRM_Follow.[Follow_Type_id]) as Follow_Type,* from CRM_Follow) as w1 group by Follow_Type");

            return DbHelperSQL.Query(strSql.ToString());
        }

        public DataSet Compared_empcusfollow(string year1, string month1, string year2, string month2, string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select hr_employee.name as yy,");
            strSql.Append(" SUM(case when YEAR( CRM_Follow.Follow_date)=('" + year1 + "') and MONTH(CRM_Follow.Follow_date)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( CRM_Follow.Follow_date)=('" + year2 + "') and MONTH(CRM_Follow.Follow_date)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" from hr_employee left outer join CRM_Follow ");
            strSql.Append(" on hr_employee.ID=CRM_Follow.employee_id ");
            strSql.Append(" where hr_employee.ID in " + idlist);
            strSql.Append(" group by hr_employee.name,hr_employee.ID ");
            strSql.Append(" order by hr_employee.ID");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 客户跟进统计
        /// </summary>
        /// <param name="year"></param>
        /// <param name="idlist"></param>
        /// <returns></returns>
        public DataSet report_empfollow(int year, string idlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append(" select name,yy,isnull([1],0) as 'm1',isnull([2],0) as 'm2',isnull([3],0) as 'm3',isnull([4],0) as 'm4',isnull([5],0) as 'm5',isnull([6],0) as 'm6',");
            strSql.Append(" isnull([7],0) as 'm7',isnull([8],0) as 'm8',isnull([9],0) as 'm9',isnull([10],0) as 'm10',isnull([11],0) as 'm11',isnull([12],0) as 'm12' ");
            strSql.Append(" from");
            strSql.Append(" (SELECT   hr_employee.ID, hr_employee.name, COUNT(derivedtbl_1.id) AS cn, YEAR(derivedtbl_1.Follow_date) AS yy, ");
            strSql.Append(" MONTH(derivedtbl_1.Follow_date) AS mm");
            strSql.Append(" FROM      hr_employee LEFT OUTER JOIN");
            strSql.Append("  (SELECT   id, employee_id, Follow_date");
            strSql.Append("  FROM      CRM_Follow");
            strSql.Append("  WHERE  ISNULL(isdelete,0)=0 and (YEAR(Follow_date) = " + year + ")) AS derivedtbl_1 ON hr_employee.ID = derivedtbl_1.employee_id");
            strSql.Append(" WHERE hr_employee.ID in " + idlist);
            strSql.Append(" GROUP BY hr_employee.ID, hr_employee.name, YEAR(derivedtbl_1.Follow_date), MONTH(derivedtbl_1.Follow_date)) as tt");
            strSql.Append(" pivot");
            strSql.Append(" (sum(cn) for mm in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]))");
            strSql.Append(" as pvt");

            return DbHelperSQL.Query(strSql.ToString());
        }


        #endregion  ExtensionMethod
    }
}

