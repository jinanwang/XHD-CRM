﻿/*
* CRM_contract.cs
*
* 功 能： N/A
* 类 名： CRM_contract
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 20:49:53    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:CRM_contract
    /// </summary>
    public class CRM_contract
    {
        #region  BasicMethod	

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_contract model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into CRM_contract(");
            strSql.Append(
                "Contract_name,Serialnumber,Customer_id,C_depid,C_empid,Contract_amount,Pay_cycle,Start_date,End_date,Sign_date,Customer_Contractor,Our_Contractor_depid,Our_Contractor_id,Creater_id,Creater_name,Create_time,Main_Content,Remarks,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append(
                "@Contract_name,@Serialnumber,@Customer_id,@C_depid,@C_empid,@Contract_amount,@Pay_cycle,@Start_date,@End_date,@Sign_date,@Customer_Contractor,@Our_Contractor_depid,@Our_Contractor_id,@Creater_id,@Creater_name,@Create_time,@Main_Content,@Remarks,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Contract_name", SqlDbType.VarChar, 250),
                new SqlParameter("@Serialnumber", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@C_depid", SqlDbType.Int, 4),
                new SqlParameter("@C_empid", SqlDbType.Int, 4),
                new SqlParameter("@Contract_amount", SqlDbType.Float, 8),
                new SqlParameter("@Pay_cycle", SqlDbType.Int, 4),
                new SqlParameter("@Start_date", SqlDbType.VarChar, 250),
                new SqlParameter("@End_date", SqlDbType.VarChar, 250),
                new SqlParameter("@Sign_date", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer_Contractor", SqlDbType.VarChar, 250),
                new SqlParameter("@Our_Contractor_depid", SqlDbType.Int, 4),
                new SqlParameter("@Our_Contractor_id", SqlDbType.Int, 4),
                new SqlParameter("@Creater_id", SqlDbType.Int, 4),
                new SqlParameter("@Creater_name", SqlDbType.VarChar, 250),
                new SqlParameter("@Create_time", SqlDbType.DateTime),
                new SqlParameter("@Main_Content", SqlDbType.VarChar, -1),
                new SqlParameter("@Remarks", SqlDbType.VarChar, -1),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.Contract_name;
            parameters[1].Value = model.Serialnumber;
            parameters[2].Value = model.Customer_id;
            parameters[3].Value = model.C_depid;
            parameters[4].Value = model.C_empid;
            parameters[5].Value = model.Contract_amount;
            parameters[6].Value = model.Pay_cycle;
            parameters[7].Value = model.Start_date;
            parameters[8].Value = model.End_date;
            parameters[9].Value = model.Sign_date;
            parameters[10].Value = model.Customer_Contractor;
            parameters[11].Value = model.Our_Contractor_depid;
            parameters[12].Value = model.Our_Contractor_id;
            parameters[13].Value = model.Creater_id;
            parameters[14].Value = model.Creater_name;
            parameters[15].Value = model.Create_time;
            parameters[16].Value = model.Main_Content;
            parameters[17].Value = model.Remarks;
            parameters[18].Value = model.isDelete;
            parameters[19].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_contract model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update CRM_contract set ");
            strSql.Append("Contract_name=@Contract_name,");
            strSql.Append("Serialnumber=@Serialnumber,");
            strSql.Append("Customer_id=@Customer_id,");
            strSql.Append("C_depid=@C_depid,");
            strSql.Append("C_empid=@C_empid,");
            strSql.Append("Contract_amount=@Contract_amount,");
            strSql.Append("Pay_cycle=@Pay_cycle,");
            strSql.Append("Start_date=@Start_date,");
            strSql.Append("End_date=@End_date,");
            strSql.Append("Sign_date=@Sign_date,");
            strSql.Append("Customer_Contractor=@Customer_Contractor,");
            strSql.Append("Our_Contractor_depid=@Our_Contractor_depid,");
            strSql.Append("Our_Contractor_id=@Our_Contractor_id,");
            strSql.Append("Main_Content=@Main_Content,");
            strSql.Append("Remarks=@Remarks");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@Contract_name", SqlDbType.VarChar, 250),
                new SqlParameter("@Serialnumber", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer_id", SqlDbType.Int, 4),
                new SqlParameter("@C_depid", SqlDbType.Int, 4),
                new SqlParameter("@C_empid", SqlDbType.Int, 4),
                new SqlParameter("@Contract_amount", SqlDbType.Float, 8),
                new SqlParameter("@Pay_cycle", SqlDbType.Int, 4),
                new SqlParameter("@Start_date", SqlDbType.VarChar, 250),
                new SqlParameter("@End_date", SqlDbType.VarChar, 250),
                new SqlParameter("@Sign_date", SqlDbType.VarChar, 250),
                new SqlParameter("@Customer_Contractor", SqlDbType.VarChar, 250),
                new SqlParameter("@Our_Contractor_depid", SqlDbType.Int, 4),
                new SqlParameter("@Our_Contractor_id", SqlDbType.Int, 4),
                new SqlParameter("@Main_Content", SqlDbType.VarChar, -1),
                new SqlParameter("@Remarks", SqlDbType.VarChar, -1),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.Contract_name;
            parameters[1].Value = model.Serialnumber;
            parameters[2].Value = model.Customer_id;
            parameters[3].Value = model.C_depid;
            parameters[4].Value = model.C_empid;
            parameters[5].Value = model.Contract_amount;
            parameters[6].Value = model.Pay_cycle;
            parameters[7].Value = model.Start_date;
            parameters[8].Value = model.End_date;
            parameters[9].Value = model.Sign_date;
            parameters[10].Value = model.Customer_Contractor;
            parameters[11].Value = model.Our_Contractor_depid;
            parameters[12].Value = model.Our_Contractor_id;
            parameters[13].Value = model.Main_Content;
            parameters[14].Value = model.Remarks;
            parameters[15].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_contract ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from CRM_contract ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select id,Contract_name,Serialnumber,Customer_id,C_depid,C_empid,Contract_amount,Pay_cycle,Start_date,End_date,Sign_date,Customer_Contractor,Our_Contractor_depid,Our_Contractor_id,Creater_id,Creater_name,Create_time,Main_Content,Remarks,isDelete,Delete_time ");
            strSql.Append(
                " ,(select Customer from CRM_Customer where id=CRM_contract.[Customer_id]) as [Customer_name] ");
            strSql.Append(" ,(select d_name from hr_department where id = CRM_contract.[C_depid]) as [C_depname]  ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_contract.[C_empid]) as [C_empname]   ");
            strSql.Append(
                " ,(select d_name from hr_department where id = CRM_contract.[Our_Contractor_depid]) as [Our_Contractor_depname]  ");
            strSql.Append(
                " ,(select name from hr_employee where id = CRM_contract.[Our_Contractor_id]) as [Our_Contractor_name]  ");
            strSql.Append(" FROM CRM_contract ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" id,Contract_name,Serialnumber,Customer_id,C_depid,C_empid,Contract_amount,Pay_cycle,Start_date,End_date,Sign_date,Customer_Contractor,Our_Contractor_depid,Our_Contractor_id,Creater_id,Creater_name,Create_time,Main_Content,Remarks,isDelete,Delete_time ");
            strSql.Append(" ,(select Customer from CRM_Customer where id=CRM_contract.[Customer_id]) as [Customer_name] ");
            strSql.Append(" ,(select d_name from hr_department where id = CRM_contract.[C_depid]) as [C_depname]  ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_contract.[C_empid]) as [C_empname]   ");
            strSql.Append(" ,(select d_name from hr_department where id = CRM_contract.[Our_Contractor_depid]) as [Our_Contractor_depname]  ");
            strSql.Append(" ,(select name from hr_employee where id = CRM_contract.[Our_Contractor_id]) as [Our_Contractor_name]  ");
            strSql.Append(" FROM CRM_contract ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM CRM_contract ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      n,id,Contract_name,Serialnumber,Customer_id,C_depid,C_empid,Contract_amount,Pay_cycle,Start_date,End_date,Sign_date,Customer_Contractor,Our_Contractor_depid,Our_Contractor_id,Creater_id,Creater_name,Create_time,Main_Content,Remarks,isDelete,Delete_time ");
            strSql_grid.Append(" ,(select Customer from CRM_Customer where id=w1.[Customer_id]) as [Customer_name] ");
            strSql_grid.Append(" ,(select d_name from hr_department where id = w1.[C_depid]) as [C_depname]  ");
            strSql_grid.Append(" ,(select name from hr_employee where id = w1.[C_empid]) as [C_empname]   ");
            strSql_grid.Append(" ,(select d_name from hr_department where id = w1.[Our_Contractor_depid]) as [Our_Contractor_depname]  ");
            strSql_grid.Append(" ,(select name from hr_employee where id = w1.[Our_Contractor_id]) as [Our_Contractor_name]  ");
            strSql_grid.Append(" FROM ( SELECT *, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from CRM_contract");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     同比环比
        /// </summary>
        /// <param name="dt1"></param>
        /// <param name="dt2"></param>
        /// <param name="idlist"></param>
        /// <returns></returns>
        public DataSet Compared_empcuscontract(string year1, string month1, string year2, string month2, string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append(" select hr_employee.name as yy,");
            strSql.Append(" SUM(case when YEAR( CRM_contract.Create_time)=('" + year1 +
                          "') and MONTH(CRM_contract.Create_time)=('" + month1 + "') then 1 else 0 end) as dt1, ");
            strSql.Append(" SUM(case when YEAR( CRM_contract.Create_time)=('" + year2 +
                          "') and MONTH(CRM_contract.Create_time)=('" + month2 + "') then 1 else 0 end) as dt2 ");
            strSql.Append(" from hr_employee left outer join CRM_contract ");
            strSql.Append(" on hr_employee.ID=CRM_contract.Our_Contractor_id ");
            strSql.Append(" where hr_employee.ID in " + idlist);
            strSql.Append(" group by hr_employee.name,hr_employee.ID ");
            strSql.Append(" order by hr_employee.ID");

            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     客户成交统计
        /// </summary>
        public DataSet report_empcontract(int year, string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                " select name,yy,isnull([1],0) as 'm1',isnull([2],0) as 'm2',isnull([3],0) as 'm3',isnull([4],0) as 'm4',isnull([5],0) as 'm5',isnull([6],0) as 'm6',");
            strSql.Append(
                " isnull([7],0) as 'm7',isnull([8],0) as 'm8',isnull([9],0) as 'm9',isnull([10],0) as 'm10',isnull([11],0) as 'm11',isnull([12],0) as 'm12' ");
            strSql.Append(" from");
            strSql.Append(
                " (SELECT   hr_employee.ID, hr_employee.name, COUNT(derivedtbl_1.id) AS cn, YEAR(derivedtbl_1.Sign_date) AS yy, ");
            strSql.Append(" MONTH(derivedtbl_1.Sign_date) AS mm");
            strSql.Append(" FROM      hr_employee LEFT OUTER JOIN");
            strSql.Append("  (SELECT   id, Our_Contractor_id, Sign_date");
            strSql.Append("  FROM CRM_contract");
            strSql.Append("  where ISNULL(isdelete,0)=0 and (YEAR(Sign_date) = " + year +
                          ")) AS derivedtbl_1 ON hr_employee.ID = derivedtbl_1.Our_Contractor_id");
            strSql.Append(" WHERE hr_employee.ID in " + idlist);

            strSql.Append(
                " GROUP BY hr_employee.ID, hr_employee.name, YEAR(derivedtbl_1.Sign_date), MONTH(derivedtbl_1.Sign_date)) as tt");
            strSql.Append(" pivot");
            strSql.Append(" (sum(cn) for mm in ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12]))");
            strSql.Append(" as pvt");

            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  ExtensionMethod
    }
}