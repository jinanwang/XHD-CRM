﻿/*
* CRM_product_category.cs
*
* 功 能： N/A
* 类 名： CRM_product_category
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 09:57:29    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     CRM_product_category:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class CRM_product_category
    {
        #region Model

        private int? _delete_id;
        private DateTime? _delete_time;
        private int _id;
        private int? _isdelete;
        private int? _parentid;
        private string _product_category;
        private string _product_icon;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string product_category
        {
            set { _product_category = value; }
            get { return _product_category; }
        }

        /// <summary>
        /// </summary>
        public int? parentid
        {
            set { _parentid = value; }
            get { return _parentid; }
        }

        /// <summary>
        /// </summary>
        public string product_icon
        {
            set { _product_icon = value; }
            get { return _product_icon; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public int? Delete_id
        {
            set { _delete_id = value; }
            get { return _delete_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}