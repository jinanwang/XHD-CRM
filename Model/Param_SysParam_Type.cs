/*
* Param_SysParam_Type.cs
*
* 功 能： N/A
* 类 名： Param_SysParam_Type
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Param_SysParam_Type:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Param_SysParam_Type
    {
        #region Model

        private DateTime? _delete_time;
        private int _id;
        private int? _isdelete;
        private string _params_name;
        private int? _params_order;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string params_name
        {
            set { _params_name = value; }
            get { return _params_name; }
        }

        /// <summary>
        /// </summary>
        public int? params_order
        {
            set { _params_order = value; }
            get { return _params_order; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}