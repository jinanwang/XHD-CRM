﻿/*
* MSG_Task.cs
*
* 功 能： N/A
* 类 名： MSG_Task
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/7/28 16:09:43    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
namespace XHD.Model
{
    /// <summary>
    /// MSG_Task:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class MSG_Task
    {
        public MSG_Task()
        { }
        #region Model
        private int _id;
        private string _task_title;
        private string _task_content;
        private int? _task_type_id;
        private int? _customer_id;
        private int? _assign_id;
        private int? _executive_id;
        private DateTime? _executive_time;
        private int? _task_status_id;
        private int? _priority_id;
        private DateTime? _remind_time;
        private int? _create_id;
        private DateTime? _create_time;
        private int? _update_id;
        private DateTime? _update_time;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string task_title
        {
            set { _task_title = value; }
            get { return _task_title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string task_content
        {
            set { _task_content = value; }
            get { return _task_content; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? task_type_id
        {
            set { _task_type_id = value; }
            get { return _task_type_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? customer_id
        {
            set { _customer_id = value; }
            get { return _customer_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? assign_id
        {
            set { _assign_id = value; }
            get { return _assign_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? executive_id
        {
            set { _executive_id = value; }
            get { return _executive_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? executive_time
        {
            set { _executive_time = value; }
            get { return _executive_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? task_status_id
        {
            set { _task_status_id = value; }
            get { return _task_status_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Priority_id
        {
            set { _priority_id = value; }
            get { return _priority_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? remind_time
        {
            set { _remind_time = value; }
            get { return _remind_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? create_time
        {
            set { _create_time = value; }
            get { return _create_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? update_id
        {
            set { _update_id = value; }
            get { return _update_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? update_time
        {
            set { _update_time = value; }
            get { return _update_time; }
        }
        #endregion Model

    }
}

