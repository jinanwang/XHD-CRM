﻿/*
* hr_post.cs
*
* 功 能： N/A
* 类 名： hr_post
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:59:28    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     hr_post
    /// </summary>
    public class hr_post
    {
        private readonly DAL.hr_post dal = new DAL.hr_post();

        #region  BasicMethod

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.hr_post model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.hr_post model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int post_id)
        {
            return dal.Delete(post_id);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string post_idlist)
        {
            return dal.DeleteList(PageValidate.SafeLongFilter(post_idlist, 0));
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     更新岗位人员
        /// </summary>
        public bool UpdatePostEmp(Model.hr_post model)
        {
            return dal.UpdatePostEmp(model);
        }

        /// <summary>
        ///     清空更新岗位人员
        /// </summary>
        public bool UpdatePostEmpbyEid(int empid)
        {
            return dal.UpdatePostEmpbyEid(empid);
        }

        #endregion  ExtensionMethod
    }
}