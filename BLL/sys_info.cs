/*
* sys_info.cs
*
* 功 能： N/A
* 类 名： sys_info
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;

namespace XHD.BLL
{
    /// <summary>
    ///     sys_info
    /// </summary>
    public class sys_info
    {
        private readonly DAL.sys_info dal = new DAL.sys_info();

        #region  Method

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.sys_info model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        #endregion  Method
    }
}