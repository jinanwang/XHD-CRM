﻿using System;
using XHD.BLL;
using XHD.Common;

namespace XHD.Controller
{
    public class sys_log
    {
        public void Add_log(int uid, string uname, string ip, string EventTitle, string EventType, int EventID,
            string Log_Content)
        {
            var log = new Sys_log();
            var modellog = new Model.Sys_log();

            modellog.EventDate = DateTime.Now;
            modellog.UserID = uid;
            modellog.IPStreet = PageValidate.InputText(ip, 255);

            modellog.EventTitle = PageValidate.InputText(EventTitle, 255);

            modellog.EventType = PageValidate.InputText(EventType, 255);
            modellog.EventID = EventID.ToString();
            modellog.Log_Content = PageValidate.InputText(Log_Content, int.MaxValue);

            log.Add(modellog);
        }
    }
}